# Project overview
---
The goal of this project is to build a monitorless baretop box for retrogaming.
The box has:
- 10 buttons, 8 for actions + start and select.
- an arcade stick
- a 3.2" LCD display to play without connecting the HDMI monitor
- a panel usb plug to connect a pad for a second player
- a panel HDMI plug to connect the box to the HDMI monitor
- a 5v power plug to supply power to raspberry, led and
- at the moment a speaker for the music when the HDMI is not plugged is missing. The earphones can be used instead.
- a power off button

![Final1](./files/Final1.jpg)
![Final2](./files/Final2.jpg)
![Final3](./files/Final3.jpg)
![Final4](./files/Final4.jpg)
![Final5](./files/Final5.jpg)

<br><br><br><br>

---
# Shopping list
---

The items to build this retrogaming box are:
- 1 raspberry Pi 3 (or higher) with at least 2GB of RAM
- 1 micro 32GB SD card (dimension can differ according to the amount of games)
- 1 zero delay encoder
- 14 connection cables (they come with the zero-delay). 10 for the buttons + 4 for the joystick or 10 + 1 joystick (come with the zero delay encoder)
- 1 USB male A cable to 4-pin Molex connector to connect zero delay encoder with Raspberry
- 10 mechanical buttons:
  - 1 plastic button with a threaded cylinder
  - 1 ring to fix the button
  - 1 mechanical relay
- 1 joystick:
  - 1 joystick
  - 4 mechanical relay
- 1 switch button to mechanically turn off the Raspberry, instead of doing it via Software
- 1 panel hdmi port to connect the external monitor
- 2 panel usb port to connect the power and an additional gaming pad
- 1 3.2inch RPi display 320x240 Pixel
- 26 pin M/F cable to connect the 3.2inch monitor to the Raspberry
- 1 usb power adapter
- 2 hdmi cables:
  - 1 short to internally connect the panel hdmi port to the Raspberry
  - 1 long to connect the panel hdmi port to the monitor
- 3 usb cables:
  - 1 short to internally connect the additional gamepad panel usb port to the Raspberry
  - 1 short to internally connect the power panel usb port to the Raspberry
  - 1 long to externally connect the power to the adapter
- 1 usb/micro-usb adapter for the internal power connection.

![components](./files/components.jpg)

<br><br><br><br>

---
# Hardware assembly
---

This section explains how to assembly all the components and it's divided in main areas:
- case
- buttons
- zero delay
- raspberry
- monitor
- connectors

<br>

## Case
The can be done using ready-made boxes or building it from scratch. It can be of any wanted dimension, in my case I used a magnum wine-box.
The box has been drilled to prepare the wholes for the buttons according to the following schema
![ButtonSchema](./files/ButtonSchema.png)
The schema has been drawn to ergonomically place the buttons and the joystick.
A *pdf* is available for printing
[PulsantieraArcadeStick_v2](./files/PulsantieraArcadeStick_v2.pdf)
And a *dxf* file is available for any desired modification
[PulsantieraArcadeStick_v2](./files/PulsantieraArcadeStick_v2.dxf)
The dotted-line border is the button border, the circle inside is the whole size.
The measurements are in the *dxf* file.
- For the buttons: the whole diameter is 28mm
- For the joystick: the whole is 9mm

Once the schema has been printed it's convenient to draw it on the woodden case marking the circle centers to be sure to drill them properly.

I added other 2 extra button wholes on the left side. I placed them without any schema but keeping them at the same distance I used to for the previously done buttons.
![case5](./files/case5.jpg)

Then I created 2 wholes on the front to place a panel usb port for the power and a panel hdmi port for the video. The size depends on the panel usb, mine were 22mm of diameter.
![case3](./files/case3.jpg)

Finally I created a little whole on the right side for a power-down button (1.5mm) o and another bigger for a gamepad usb connection (this time the panel usb was 24mm).
![case4](./files/case4.jpg)

In my project I also put a small 3.2-inch monitor so between the joystick and the buttons I cut a place to fit the display connector. This is 35mmx7mm but, again, it depends on the display.
![monitorCut](./files/monitorCut.jpg)

Once the drill has been done I used a primer on the case to feed and preserve the wood.
![case1](./files/case1.jpg)
![case2](./files/case2.jpg)

<br>

## Buttons
After that I placed the buttons in the whole fixing them by the ring and placing the relay below each of them.
I used the cables from the zero-delay board to connect the relay to the board.
I put the red cable on the common *COM* and the black one on normally open *NO*.
![buttonConnection](./files/buttonConnection.jpg)

<br>

## Zero Delay Encoder
This board comes with pin connectors and it connects joystick and buttons to the Raspberry Pi without introducing delay in the command.
The connection schema is in the encoder documentation.
[ZeroDelayEncoder](./files/Datasheet_JOY-iT_Zero_Delay_Encoder.pdf)
In this project the joystick connector is used instead of the single up-down-left-right pins.

<br>

## Monitor
The chosen LCD monitor has a 26 pin connector that must be connected with the first pins on Raspberry Pi GPIO.
The first Raspberry Pi pins are from 1 to 26 and they are located at the opposite side of usb ports.
The connection must figure like this
![LCDconnection](./files/LCDconnection.jpg)
But in the project I used pin cables as the monitor is not directly placed on the Raspberry but it's on the wodden case.
![LCDconnectionByCable](./files/LCDconnectionByCable.jpg)
![WiringMonitor](./files/WiringMonitor.jpg)


<br><br><br><br>

---
# RetroPie
---

As operating system for the Raspberry Pi I choose ***RetroPie*** which is based on *Raspbian*.
In this section there are the commands for the basic RetroPie installation and configuration.

<br>

## Installation
As first step the OS must be downloaded chosing the right version, according to the Raspberry Pi model (mine is 3). The site is  by
> *https://retropie.org.uk/download/*

To verify the download has been succesfully executed, without any corruption, the *md5* of the file must be compared with the one on the site.
To generate the *md5* of the downloaded file the command is
```shell
$ md5sum Downloads/retropie-buster-4.7.1-rpi2_3.img.gz
```
This command, as well as the following ones, has been issued for my situation:
- the downloaded file is retropie-buster-4.7.1-rpi2_3.img.gz
- the folder where it was placed was Downloads
- I issued the command from my home (Downloads is a home's subfolder)

Then the image is compressed in a gz file so it must be extracted
```shell
$ gunzip Downloads/retropie-buster-4.7.1-rpi2_3.img.gz
```

Now the image must be written on an SD card using *dd* command
```shell
$ sudo dd if=retropie-buster-4.7.1-rpi2_3.img of=/dev/rdisk4 bs=1m
```
The default login is
- username: *pi*
- password: *raspberry*

<br>

## RetroPie setup
After the installation succesfully finished there are few standard steps to perform.
The RetroPie system start up the Raspberry with a graphical interface. The following steps can be done via interface or moving to the shell using *ALT+F1*.
In any case the setup uses an old-style graphical interface.
In this case I went to the shell.

Now to open the retropie setup system the command is
```shell
$ sudo raspi-config
```
![raspiconfig](./files/raspiconfig.png)

The steps to perform here are
- increase the available memory size: this is necessary otherwise is not possible to use the full SD size, so you are seriously limited in the number of games you can put on it.
- setup the locale: optional but useful if you use the shell
- setup the network: it depends on your network, it can be necessary in case you don't have a dchp
- enable the ssh: it is useful to connect and modify to the station without dismount the Raspberry Pi or connect it to the laptop.
- change the user password: as the system will be connected to the network is a best practice to change the default password

It is also possible to update the system from the menu, otherwise it is possible by shell
```shell
$ sudo apt update && apt upgrade
```

The *Advanced Options* entry allows to extend the memory but also to play with audio and video setup. I left them as by default.




<br><br><br><br>

---
# Connecting the LCD
---

This project has been realized using a cheap 3.2" LCD monitor

## Installing the driver
The required driver is ***ili9341***.
The driver can be installed using the goodtft GitHub's repository.
The following instruction are also on the repository.
```shell
$ git clone https://github.com/goodtft/LCD-show.git
```
Grant the proper rights to the folder and its content
```shell
$ chmod -R 755 LCD-show
```

Get in the folder and execute the command related to the appropriate monitor size.
```shell
$ cd LCD-show
$ ./LCD32-show
```

After that the LCD should display a tty but it doesn't display the RetroPie interface.
To do that the chosen way is to duplicate the main video buffer stream on a secondary stream.

<br>

## Duplicating the HDMI buffer stream on the LCD
This step is needed to see the screen and allow to play from the LCD display whether the HDMI monitor is connected or not.
To accomplish this task a program from AndrewFromMelbourne's GitHub repository has been used:

From the base directory, in this project is the ***/home/pi*** , execute the following step to clone the repository

```shell
$ git clone https://github.com/AndrewFromMelbourne/raspi2fb.git
```

The command will create a folder with the project name.
The following instruction are also on the repository.

<br>

### Installing depenences
Install/update the build system generator
```shell
$ sudo apt install cmake
```

Install the needed libraries
```shell
$ sudo apt-get install libbsd-dev
```

<br>

### Installing the application
Get inside the working directory ***/home/pi/raspi2fb*** folder and follow the steps to compile
```shell
$ mkdir build
$ cd build
$ cmake ..
$ make
```

After compiled the following commands install the application and copy some file to set it as service
```shell
$ sudo make install
$ sudo cp ../raspi2fb.init.d /etc/init.d/raspi2fb
$ sudo update-rc.d raspi2fb defaults
$ sudo service raspi2fb start
```
<br>

### Configuring
The settings for the secondary framebuffer can be modified via following command:
```shell
$ raspi2fb [options]
```

The daemon can be started manually by this command but it should start automatically after a restart.

<br>

## Rotate the display
In case there's the need of rotating the display it can be done in the file
> */boot/config.txt*

The display can be rotated by 90° each time
- 0 is the current rotation
- 1 is 90°
- 2 is 180°
- 3 is 270°

At the end of the file a rotate option must be added. If the display is the original one the parameter name is *lcd_rotate*, otherwise *display_rotate* can be used.
After the file has been saved with the same name the system must bee rebooted.
```shell
$ sudo nano /boot/config.txt
```
```txt
display_rotate=2
```
```shell
$ sudo reboot
```


<br><br><br><br>

---
# Create a shutdown button
---

RetroPie must be shutdown in a proper way. This can be done navigating throught the interface or, easily, creating a shutdown button.
This button is connected to the Raspberry GPIO and the action is intercepted and triggers a python script.

<br>

## Scripting
This section contains 2 scripts:
- python script: is the script to shutdown effectively the RetroPie
- shell script: is the script which listen for the button event


<br>

### Shell
The script is named *listen4shutdown.sh* and it's located under
> */etc/init.d/listen4shutdown.sh*

It executes the python and it creates a service. The content is
```shell
#! /bin/sh

### BEGIN INIT INFO
# Provides:          listen-for-shutdown.py
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

# If you want a command to always run, put it here

# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting listen4shutdown.py"
    /usr/local/bin/listen4shutdown.py &
    ;;
  stop)
    echo "Stopping listen4shutdown.py"
    pkill -f /usr/local/bin/listen4shutdown.py
    ;;
  *)
    echo "Usage: /etc/init.d/listen4shutdown.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
```
It can be activated as service with the following command:
```shell
$ sudo /etc/init.d/listen4shutdown start
```
It can be de-activated with
```shell
$ sudo /etc/init.d/listen4shutdown stop
```
<br>

### Python
The python script is the one who executes the shutdown as it manages the GPIO pin by dedicated libraries. It must be located, consistently with the *shell* script, in the following path
> */usr/local/bin/listen4shutdown.py*

The script is the following
```py
#!/usr/bin/env python

import RPi.GPIO as GPIO
import subprocess

GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.wait_for_edge(21, GPIO.FALLING)

subprocess.call(['shutdown', '-h', 'now'], shell=False)
```

<br>

## Electrical wiring
The shutdown button is a release switch connected to PIN 40 (input) and PIN 39 (ground) of the GPIO.
A 100ohm pull-up resistor has been connected to between the switch and the PIN 40 to protect the system from garbage signals.

## Software
The signal triggered by the button is intercepted by a service

<br>

## Turn-on button
This function can be done activating the reset on the Raspberry board by P6 header. The header must be connected to a release switch to short-circuit it. This cause a reset:
- a turn-on if it has been shut down but is still connected to the power
- a wrong shutdown if is still on

In this project there is no power supply button but a safer solution could be to switch off the power and then on again.

<br><br><br><br>

---
# Game Installation
---

The games are divided by emulators, each emulator has its own folder. The roms of the games must be placed in the appropriate folder.
The roms folder is in the user's home. The user by default is ***pi***:
> */home/pi/RetroPie/roms*

The roms should be taken automatically, without any need of restarting the Raspberry

<br>

## Add an emulator
RetroPie comes with a number of emulators already installed. Others can be added by configuration tool inside the graphical RetroPie interface
![AddEmulator1](./files/AddEmulator1.png)
or by command line. From the *pi* user's *home*
```shell
$ cd RetroPie-Setup
$ sudo ./retropie_setup.sh
```

In both cases an old-style menu prompts up. To add the emulator the *Manage Packages* entry must be taken
![AddEmulator2](./files/AddEmulator2.png)
All the packages are listed here, including the emulators. The desired one can be selected
![AddEmulator3](./files/AddEmulator3.png)
Now the suggested option is to use a pre-compiled binary
![AddEmulator4](./files/AddEmulator4.png)

> **N.B.:** the package name is the emulator name. Each console has more than one possible emulator. The names are on the RetroPie Docs site under *Emulators*
> *https://retropie.org.uk/docs/*

To remove a package the procedure is the same but, instead of *Install from binary*, the *Remove* option must be used.

A reboot is needed.

<br>

## Package multi-disk games to a single file
This is mainly for Playstation 1 (PSX or PSP).
The best option is to get a *.PBP* file as it is a compressed format which saves about 30% of memory.
To get multiple image in to one there is a tool named *PSXPackager* and it can be found at
> *https://github.com/RupertAvery/PSXPackager*

> **N.B.:** The *.PBP* conversion can be done also for single disk games.

<br>

## SCUMMVM
SCUMMVM stands for Script Creation Utility for Maniac Mansion Virtual Machine.
It works in a slightly different way from other emulator.

To use it the package must be installed (see *Add an emulator* paragraph) as it is optional.
From *retropie_setup.sh* script the *opt* option must be selected.
The package to install is the *142* which is ***scummvm-sdl1***.
This package has a better audio compatibility compared to the standard one, this avoids some crash.

Each game has its own fileset to group in a folder. The files may change according to the platform they come from. The needed files are listed in
> *http://wiki.scummvm.org/index.php/Datafiles*

The game files must be placed in a *roms* subfolder named *scummvm*
After running the loader script *+Start ScummVM-SDL1.sh* the SCUMMVM interface is open. The games must be loaded and can be executed from here. However a *.svm* file is created in the root. This creates an entry that can be used to directly execute the game from RetroPie interface.

The emulstation must be restarted.

<br>

## Neo-Geo
The *neogeo* emulator requires the bios is placed together with the roms in
> *roms/neogeo/*

folder. The bios name must be *neogeo.zip*

According to the game roms (MVS/AES or CD), the bios can be downloaded from
> *http://unibios.free.fr/download.html*

After the download the unibios can be renamed in *neogeo.zip*
> **N.B.:** It is only for personal usage and not for commercial projects

<br><br><br><br>

---
# Game List
---

In this section there's the list of personal recommendations divided by console name.

> **N.B.:** As disclaimer, the content of this project doesn't include any rom of any game. Some of them may be free and they can be found on internet, other may be protected by copiright so they must be bought.

Some roms are downloadable in:
- Rom Hustler: http://romhustler.net/
- Free ROMs: https://www.freeroms.com/
- Cool ROM: http://coolrom.com/
- Coleco Vision Addict: http://cvaddict.com/list.php

<br>

## Colecovision (coleco) - emulator added as it was optional
- Donkey Kong Jr.
- Frogger
- Jetpack
- Multiverse
- Pac-Man
- Princess Quest
- Qbert
- Star Trek Strategic Operations Simulator
- Star Wars The Arcade Game

<br>

## Game Boy Advance (gba)
- 007 - Everything or Nothing
- Action Man - Robot Attack
- Advance GT2
- Advance GTA
- Alex Rider - Stormbreaker
- Altered Beast - Guardian of the Realms
- Asterix & Obelix - Bash Them All!
- Asterix & Obelix XXL
- Astro Boy - Omega Factor
- Baldur's Gate - Dark Alliance
- Batman Begins
- Batman - Rise of Sin Tzu
- Batman Vengeance
- Breath of Fire
- Breath of Fire II
- Broken Sword - The Shadow of the Templars
- Bruce Lee - Return of the Legend
- Bubble Bobble - Old & New
- Buffy - The Vampire Slayer - Wrath of the Darkhul King
- Car Battler Joe
- Cars
- Castelvania - Aria of Sorrow
- Castelvania - Circle of the Moon
- Castelvania - Harmony of Dissonance
- Castleween
- Catwoman
- Charlie and the Chocolate Factory
- Colin McRae Rally 2.0
- Comix Zone
- Crash Bandicoot Fusion
- Crash Bandicoot Purple - Ripto's Rampage
- Crash Bandicoot - The Huge Adventure
- Creatures
- Custom Robo GX
- Daredevil
- Defender
- DemiKids - Dark Version
- Digimon - Battle Spirit
- Digimon - Battle Spirit 2
- Dinotopia - The Timestone Pirates
- Double Dragon Advance
- Duke Nukem Advance
- E.T. - The Extra-Terrestrial
- Eragon
- Extreme Ghostbusters - Code Ecto-1
- Fantastic 4
- Fantastic 4 - Flame On
- Final Fantasy I & II - Dawn of Souls
- Final Fantasy IV Advance
- Final Fantasy V Advance
- Final Fantasy VI Advance
- Final Fight One
- Garfield and His Nine Lives
- Gremlins - Stripe vs. Gizmo
- Harry Potter and the Chamber of Secrets
- Harry Potter and the Goblet of Fire
- Harry Potter and the Prisoner of Azkaban
- Harry Potter and the Sorcerer's Stone
- Harry Potter - Quidditch World Cup
- I Fantastici 4
- Gli Incredibili
- Inspector Gadget - Advance Mission
- Inspector Gadget Racing
- Inuyasha - Naraku no Wana! Mayoi no Mori no Shoutaijou
- The Invincible Iron Man
- Jurassic Park III - Dino Attack
- Jurassic Park III - Park Builder
- Justice League Heroes - The Flash
- Justice League - Injustice for All
- King of Fighters EX2, The - Howling Blood
- Konami Collector's Series - Arcade Advanced
- Lady Sia
- Legend of Spyro, The - A New Beginning
- Legend of Zelda, The - A Link to the Past & Four Swords
- Legend of Zelda, The - The Minish Cap
- Lord of the Rings, The - The Fellowship of the Ring
- Lord of the Rings, The - The Return of the King
- Lord of the Rings, The - The Third Age
- Lord of the Rings, The - The Two Towers
- Lucky Luke - Wanted!
- Mario & Luigi - Superstar Saga
- Marvel - Ultimate Alliance
- Max Payne Advance
- Mazes of Fate
- Medabots AX - Metabee Version
- Megaman & Bass
- Megaman Battle Network 6 - Cybeast Falzar
- Megaman Battle Network 6 - Cybeast Gregar
- Megaman Zero 4
- Metalgun Slinger
- Metal Slug Advance
- Metroid Fusion
- Metroid - Zero Mission
- Midnight Club - Street Racing
- Minority Report - Everybody Runs
- Monster House
- Monsters, Inc.
- The Mummy
- Naruto - Ninja Council 2
- Need for Speed Carbon - Own the City
- Need for Speed - Most Wanted
- Need for Speed - Porsche Unleashed
- Need for Speed - Underground 2
- Ninja Cop
- Oddworld - Munch's Oddysee
- Peter Pan - Return to Neverland
- Peter Pan - The Motion Picture Event
- Pink Panther - Pinkadelic Pursuit
- Pirates of the Caribbean - Dead Man's Chest
- Pirates of the Caribbean - The Curse of the Black Pearl
- Planet Monsters
- Planet of the Apes
- Power Rangers - Dino Thunder
- Power Rangers - Ninja Storm
- Power Rangers - S.P.D.
- Power Rangers - Time Force
- Power Rangers - Wild Force
- Prince of Persia - The Sands of Time
- Robotech - The Macross Saga
- Robots
- Sabre Wulf
- Scorpion King, The - Sword of Osiris
- SD Gundam Force
- Shining Soul
- Shining Soul II
- Shrek 2 - Beg for Mercy
- Shrek - Hassle at the Castle
- Shrek - Reekin' Havoc
- Shrek - Smash n' Crash Racing
- The Sims - Bustin' Out
- Sonic Advance 2
- Spider-Man
- Spider-Man 2
- Spider-Man 3
- Spider-Man - Battle for New York
- Spider-Man - Mysterio's Menace
- SpongeBob SquarePants - Battle for Bikini Bottom + Jimmy Neutron - Boy Genius
- SpongeBob SquarePants - SuperSponge + Revenge of the Flying Dutchman
- SpongeBob SquarePants - The Movie + Freeze Frame Frenzy
- Star Wars Episode II - Attack of the Clones
- Star Wars Episode III - Revenge of the Sith
- Star Wars - Flight of the Falcon
- Star Wars - Jedi Power Battles
- Star Wars - The New Droid Army
- Star Wars Trilogy - Apprentice of the Force
- Steel Empire
- Stuntman
- Sword of Mana
- Teenage Mutant Ninja Turtles
- Teenage Mutant Ninja Turtles 2 - Battle Nexus
- Teen Titans
- Teen Titans 2 - The Brotherhood's Revenge
- Tekken Advance
- Terminator 3 - Rise of the Machines
- Tim Burton's Nightmare Before Christmas, The - The Pumpkin King
- Tiny Toon Adventures - Buster's Bad Dream
- Tiny Toon Adventures - Scary Dreams
- TMNT
- Tomb Raider - Legend
- Tomb Raider - The Prophecy
- Tom Clancy's Splinter Cell
- Tom Clancy's Splinter Cell - Pandora Tomorrow
- Treasure Planet
- Tron 2.0 - Killer App
- Turok Evolution
- Van Helsing
- WarioWare - Twisted!
- Wing Commander - Prophecy
- X2 - Wolverine's Revenge
- X-Men III - The Official Game
- X-Men - Reign of Apocalypse
- xXx
- Yu Yu Hakusho - Spirit Detective
- Zoids Legacy

<br>

## Game Boy Color (gbc)
- Animorphs
- Buffy the Vampire Slayer
- Dragon Warrior I & II
- Duke Nukem (2D)
- The Legend of Zelda - Link's Awakening DX
- The Legend of Zelda - Oracle of Seasons

<br>

## Nintendo 64 (n64)
- Bomberman 64
- Bomberman Hero
- Castelvania
- Castelvania - Legacy of Darkness
- Command & Conquerer
- Daffy Duck Starring as Duck Dodgers
- Doom 64
- Duke Nukem 64
- Duke Nukem Zero Hour
- Elmo's Letter Adventure
- Elmo's Number Journey
- Forsaken 64
- Indiana Jones and the Infernal Machine
- Legend of Zelda: The Majora's Mask
- Legend of Zelda: The - Ocarina of Time
- Magical Tetris Challenge
- Mario Golf
- Mario Kart 64
- Mario Party 3
- Off Road Challenge
- Paper Mario
- Power Rangers - Lightspeed Rescue
- Quake 64
- Quake II
- Quest 64
- Robotech - Crystal Dreams
- Rocket - Robot on Wheels
- Rugrats in Paris - The Movie
- South Park
- South Park - Chef's Luv Shack
- South Park Rally
- StarCraft 64
- Star Wars Episode I - Battle for Naboo
- Star Wars Episode I - Racer
- Star Wars - Rogue Squadron
- Super Mario 64
- Super Robot Spirits
- Super Robot Taisen 64
- Taz Express
- Tetris 64
- Tom and Jerry in Fists of Furry
- Tom Clancy's Rainbow Six
- Toy Story 2 - Buzz Lightyear to the Rescue!
- Transformers - Beast Wars Transmetals
- Turok 2 - Seeds of Evil
- Turok 3
- Turok - Dinosaur Hunter
- Turok - Rage Wars
- Vigilante 8
- Vigilante 8 - 2nd Offense
- Wipeout 64
- Worms Armageddon
- Xena Warrior Princess - The Talisman of Fate
- Yoshi's Story

<br>

## Neo-Geo (neogeo)
- Blazing Star
- King of Fighters
- Metal Slug 3
- Metal Slug X

<br>


## Nintendo Entertainment System (nes)
- The Blues Brothers
- Donkey Kong
- Super Mario Bros
- Super Mario Bros 3
- Teenage Mutant Ninja Turtle

<br>

## TurboGrafx-16 (pcengine)
- Aldynes
- Batman
- Bonk's Adventure
- Bonk's Revenge
- Bravoman
- Burning Angels
- Cadash
- City Hunter
- Darkwing Duck
- Doraemon
- Dungeon Explorer
- Ghost Manor
- Jackie Chan's Action Kung Fu
- Knight Rider Special
- Night Creatures
- Tale Spin
- The Legendary Axe
- The Legendary Axe II
- Son Son II
- Splatterhouse

<br>


## Play Station (psx) (todo)
- Alundra 2
- Breath of Fire III
- Carmageddon
- Castlevania X: Symphony of the Night
- Dragon Valor
- Final Fantasy IV
- Final Fantasy VII
- Gran Turismo
- GrandiaThe Blues Brothers
- GTA
- Legend of the Dragoon
- Metal Gear Solid
- Myst
- Parasite Eve
- Parasite Eve 2
- Resident Evil
- Silent Hill
- Suikoden
- Tenchu
- Wild Arms
- Xenogears

<br>

## SCUMMVM (todo) - emulator added as it was optional
- Broken Sword 1
- Broken Sword 2
- Day of the Tentacle
- Indiana Jones and the Fate of Atlantis
- Indiana Jones and the Last Crusade
- King's Quest V
- King's Quest VI
- King's Quest VII
- The Legend of Kyrandia
- The Legend of Kyrandia 2: The Hand of Fate
- The Legend of Kyrandia 3: Malcom's Revenge
- Loom
- Lure of the Temptress
- Simon The Sorcerer
- Simon The Sorcerer 2
- The Secret Of Monkey Island 1
- The Secret Of Monkey Island 2
- The Secret Of Monkey Island 3
- Zak McKracken and the Alien Mindbenders

<br>

## Sega Master System (mastersystem)
- Alex Kidd BMX Trial
- Alex Kidd in High Tech World
- Alex Kidd in Miracle World
- Bart vs. the Space Mutants
- Bart vs. the World
- Bram Stoker's Dracula
- Bubble Bobble
- Cyber Shinobi
- Cyborg Hunter
- Daffy Duck in Hollywood
- Dick Tracy
- Dynamite Dux
- Ecco the Dolphin
- The Flash
- Ghostbusters
- Ghouls 'n Ghosts
- Ghost House
- Heroes of the Lance
- Kung Fu Kid
- Laser Ghost
- Master of Darkness
- Montezuma's Revenge
- Predator 2
- Psychic World
- Robocop 3
- Robocop vs. Terminator
- Shadow dancer
- Spider-man - Return of the Sinister Six
- Spider-man vs. the Kingpin
- Spy vs. Spy
- Strider 2
- Vampire
- Xenon 2
- X-Men Mojo World
- Zaxxon 3D

<br>

## Sega Mega Drive (megadrive)
- Aladdin
- Alisa Dragoon
- Altered Beast
- Animaniacs
- Another World
- Atomic Robo Kid
- Bastard
- Batman 3
- Batman Returns
- Battletoads and Double Dragon
- Brutal: Paws of Fury
- Captain America and the Avengers
- Castlevania - Bloodlines
- Castlevania - The New Generation
- Double Dragon 5
- Duke Nukem 3D
- Frogger
- Gargoyles
- Golden Axe
- Golden Axe 3
- Hercules
- Hook
- Indiana Jones and the Last Crusade
- Joe and Mac Caveman Ninja
- Justice League Task Force
- Marsupilami
- Mary Shelly's Frankenstein
- Mazin Saga
- Megaman
- Mega Turricane
- Michael Jackson's Moonwalker
- Mickey Mania - Timeless Adventures of Mickey Mouse
- Mickey Mouse - Castle of Illusion
- Mickey Mouse - Fantasia
- Mickey Mouse - Great Circus Mystery
- Mickey Mouse - World of Illusion
- Mickey's Ultimate Challenge
- Micro Machines
- Micro Machines 2
- Mortal Kombat 2
- Ms. Pac-Man
- Nightmare Circus
- No Escape
- Pinocchio
- Prince of Persia
- Sailor Moon
- Scoobiydoo Mistery
- Sonic the Hedgehog 2
- Splatter House 3
- Stargate
- Star Trek - Deep Space 9 - Crossroads of Time
- Star Trek - The Next Generation
- Stormlord
- Streets of Rage 3
- Superman
- Tale Spin
- Taz Mania
- Teenage Mutant Hero Turtles - The Hyperstone Heist
- Teenage Mutant Ninja Turtles - Return of the Shredder
- Teenage Mutant Ninja Turtles - Tournament Fighters
- Terminator 2 - Judgement Day
- Terminator 2 - The Arcade Game
- Tetris
- The Punisher
- Tom and Jerry - Frantic Antics
- Toy Story
- True Lies
- Wacky Worlds
- Warlock
- Waterworld
- Young Indiana Jones Chronicles
- Young Indiana Jones - Instrument of Chaos
- Xenon 2 Megablast
- Zombies
- Zombies Ate My Neighbors
- Zool


<br>

## Super Nintendo Entertainment System (snes)
- Actraiser
- Actraiser 2
- Addams Family
- Alien 3
- Asterix
- Biker Mice From Mars
- The Blues Brothers
- Capitan America
- Casper
- Castlevania - Dracula X
- Castlevania - Vampire's Kiss
- Cyborg 009
- Daffy Duck - The Marvin Missions
- Darius Twin
- Dark Half
- Death And Return Superman
- Double Dragon
- Donald Duck
- Donkey Kong Country 3
- Doom Troopers
- Dragon Ball Z (RPG)
- Dragon Ball Z Story
- Dragon Ball Z Superbutouden 3
- Dragon Bruce Lee
- Dragon Knight
- Dragon View
- Earthworm Jim
- Earthworm Jim 2
- Elfaria 2
- EVO
- Final Fight
- Gun Hazard
- Hokuto No Ken 6
- Illusion Of Gaia
- Inspector Gadget
- Joe and Mac 2
- Justice League
- Kinight Gudam
- Kirby's Avalanche
- Legend of Toki - The Going Ape Spit
- The Legend of Zelda
- Lemmings
- Lethal Weapon
- Macross
- The Magical Quest Starring Mickey Mouse
- Magic Sword
- Mario is Missing
- Mario's Time Machine
- Mario & Wario
- Marvel Super Heroes
- Maximum Carnage
- Megaman 7
- Megaman X
- Megaman X 3
- Mickey's Ultimate Challenge
- Mighty Morphin Power Rangers
- Ninja Gaiden Trilogy
- Nosferatu
- Pac-Man 2 - The New Adventures
- Pink Panther Goes to Hollywood
- Power Rangers Movie
- Prehistorik Man
- Puzzle Bobble
- Real Monsters
- Record Of Lodoss War
- Return of Double Dragon
- Robotrek
- Romancing Saga 3
- Sailor Moon Supers
- Secret of Mana 2
- Slam Dunk
- Sonic Blastman 2
- Spawn
- Spiderman and Venom in Separation Anxiety
- Spiderman Lethal Foes
- Spiderman X-Men Arcade's Revenge
- Star Trek - Starfleet Academy
- Street Fighter Alpha 2
- Super Bomberman 5
- Super Mario All Stars
- Super Mario Kart
- Super Mario World
- Super Mario World 2 - Yoshi's Island
- Super Metroid
- Super Pang
- Super Return of Jedi
- Super Robot Wars 4
- Super Tetris 3
- Super Turrican
- Super Turrican 2
- Tales Of Phantasia
- Tekkaman Blade
- The Terminator
- The Pagemaster
- Tinytoon Adventures
- Tom & Jerry
- Total Carnage
- Turtle Ninja 5 Tournament Fighter
- Ultima 7 Black Gate
- Ultraman
- Ultraman - Towards the Future
- Universal Soldier
- Utopia
- Vampires Kiss
- Virtua Fighter 2
- Wild Guns
- Wolfenstein 3D
- Wolverine Adamantium Rage
- Worms
- WWF Super Wrestlermania
- X-Men Mutant Apocalypse
- Yoshi's Cookie
- Young Merlin

<br>
